��    
      l      �       �   `   �   	   R     \     p     u     ~     �  K   �  Q   �  �  E  \   @  
   �     �     �  
   �     �     �  Q   
  W   \               	                
              All necessary information are available at client account SALESmanago in Settings => Integration Client ID Contact owner email Save Settings Tags after login Tags after registration Tags should be separated by comma without space ex: woocommerce_login,login Tags should be separated by comma without space ex: woocommerce_register,register Project-Id-Version: SALESmanago
POT-Creation-Date: 2014-10-23 19:50+0100
PO-Revision-Date: 2014-10-23 19:50+0100
Last-Translator: 
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.10
X-Poedit-Basepath: .
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Wszystkie potrzebne informacje są dostępne w SALESmanago w sekcji Ustawienia => Integracja ID klienta Email właściciela kontaktu Zapisz ustawienia Ustawienia Tagi po zalogowaniu Tagi po rejestracji Tagi powinny być oddzielone przecinkiem (bez spacji) np: woocommerce_login,login Tagi powinny być oddzielone przecinkiem (bez spacji) np: woocommerce_register,register 