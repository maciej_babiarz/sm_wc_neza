=== SALESmanago Marketing Automation for WooCommerce ===

Plugin URI:  https://support.salesmanago.pl/plugin-for-generating-contacts-on-wordpress-based-sites/
Contributors: Benhauer Sp. z o.o.
Tags: marketing automation, email marketing, crm, live chat, social media, lead nurturing, lead generation, personalized marketing, woocommerce, contacts, lead tracking, click tracking, visitor tracking, inbound marketing, subscription, marketing, newsletter, popup, email, contacts database, contact form, popup form
Requires at least: 4.6
Tested up to: 4.9.1
Stable tag: 1.0.1
License: GPLv2+ License
URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: cf7-salesmanago
Domain Path: /languages
Requires PHP: 5.6

SALESmanago Marketing Automation is the most innovative Marketing Automation Platform for e-commerce companies of all sizes.

=== Description ===

[SALESmanago](http://salesmanago.com/) is a cloud based online Marketing Automation platform used by over 10.000 companies all over the world that manage databases of over 300 mln customers. SALESmanago unique Digital Body Language features, real-time personalization engine enable marketers to deliver 1-to-1 offers via all marketing channels including e-mail marketing, dynamic website content, ad networks and direct sales.

[Sign up](https://www.salesmanago.com/info/registercst.htm?type=WORDPRESS) for your free account to get started.

[youtube https://www.youtube.com/watch?v=X3MsjVHHR68]

=== Installation ===

1. In WordPress administration panel navigate to ‘Plugins’ and then select ‘Add new’. Find WooCommerce and click ‘Install Now’.
2. Open the ‘Plugins’ section in WordPress adminstration panel and then go to ‘Installed plugins’. Click ‘Activate’ next to ‘WooCommerce’.
3. Click again on ‘Plugins’ and ‘Add new’. Next, select ‘Upload’ and ‘Install now’.
4. Navigate to the ‘Plugins’ section in the administration panel and open ‘Installed plugins’. Click ‘Activate’ next to SALESmanago.
5. In the main administration menu expand ‘WooCommerce’ and select ‘SALESmanago’.
6. Fill in all fields with the data that you will find in the panel of your SALESmanago account in Settings -> Integration.
7. Save the settings. From this moment on, each user, who will fill in the login or registration form,
   will be added to your SALESmanago account as a monitored contact with a relevant tag (Tags register user, Tags login user).

=== Frequently Asked Questions ===

= What is SALESmanago generally used for? =

SALESmanago is generally used for the following purposes:

- Monitoring code integration.

- Adding and deleting contacts with details.

- Adding and deleting tags.

- Management of external events.

- Management of coupons.

- Contact activity export.

- Management of contacts list.

- Sending emails.

- Management of users tasks.

- Management of automation rules.

- Monitoring AJAX/JavaScript events.

- Sending external events through JavaScript.

= Does SALESmanago integrate with any other apps? =

Yes, SALESmanago integrates with the following apps:

Ecommerce:
   Magento, PrestaShop, IAI-Shop, Shoper, Atomstore, 2ClickShop, Ebexo, and Selly.

CRM and ERP:
   Salesforce, SugarCRM, SAP Business One, LiveSpace CRM, RedmineCRM, and Berberis.

Social Media:
   Facebook, Facebook Custom Audiences, Facebook Ads, and Twitter.

Supporting tools:
   Zapier.com, Landingi.pl, Quartic, Ankietka.pl, Mysurveylab.com, Survicate, and Livechat1.pl.

Communication:
   Smsapi.pl, SerwerSMS.pl, Materna Communications, Tide software, Sit mobile, ViaNett, MailChimp, Redlink, Advanced Telecom, Euro-SMS, IMOBIS, Labyrtintti, GSMS, Sales traffic, Sinhro, Voice.com, SMS sendroimp, Esendex, Publi, Text Marketer, Spryng, Modica, InfoBIP, Twilio, Comapi, GUPSHUP, and Eurocom.

= Does SALESmanago offer guides, tutorials and/or customer support? =

Each of customers gets dedicated specialized support from our Marketing Automation specialists that helps in designing the processes and proactively helping in the implementation. SALESmanago is proud to play a part in your marketing and working with you to achieve your desired business outcomes.

Another great option is [SALESmanago e‑learning platform](http://elearning.salesmanago.com/)

During the online Marketing Automation course you will get answers to the following questions:
- What is Marketing Automation?
- How can I use the knowledge of Marketing Automation in practice?
- How can I receive the SALESmanago Marketing Automation Specialist Certificate?

=== Changelog ===

1.0.0 - Initial release
1.0.1 - purchase event fix

=== Screenshots ===

screenshot-1.png
screenshot-2.png
screenshot-3.png
screenshot-4.png
screenshot-5.png
screenshot-6.png