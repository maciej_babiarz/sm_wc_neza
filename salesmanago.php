<?php
/**
 * Plugin Name: SALESmanago
 * Description: SALESmanago Marketing Automation integration for WooCommerce
 * Version: 1.0.2
 * Author: SALESmanago
 * Author URI: http://www.salesmanago.pl
 * License: GPL2
 */

/*  Copyright 2014  SALESmanago  (email : support@salesmanago.pl)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
//add scripts
function salesmanago_script()
{
    // Enqueue monitor script
    wp_enqueue_script('wc-monitor', plugins_url('js/monitor.js', __FILE__), array(), false, true);

    $options = get_option('pb_salesmanago_options');
    wp_localize_script('wc-monitor', '_smid', $options['pb_salesmanago_client_id']);
    wp_localize_script('wc-monitor', '_smEndpoint', $options['pb_salesmanago_end_point']);
    if (isset($_COOKIE['smclient'])) wp_localize_script('wc-monitor', '_smContactId', strip_tags($_COOKIE['smclient']));
}

add_action('wp_enqueue_scripts', 'salesmanago_script');

//add language
function salesmanago_language_init()
{
    load_plugin_textdomain('wco-salesmanago', false, dirname(plugin_basename(__FILE__)) . '/languages');
}

add_action('init', 'salesmanago_language_init');

//add submenu to option page
add_action('admin_menu', 'pb_salesmanago_add_submenu_page');
function pb_salesmanago_add_submenu_page()
{
    @add_submenu_page(woocommerce, salesmanago, SALESmanago, manage_options, salesmanago, pb_salesmanago_options_page);
}

//generate option page
function pb_salesmanago_options_page()
{
    ?>
    <div class="wrap">
        <h2>SALESmanago Marketing Automation</h2>
        <form action="options.php" method="post"
              enctype="multipart/form-data"> <?php settings_fields('pb_salesmanago_main'); ?>
            <?php settings_fields('pb_salesmanago_options'); ?>
            <?php do_settings_sections('salesmanago'); ?>
            <input name="Submit" type="submit" value="<?php echo __('Save', 'wco-salesmanago'); ?>"/>
        </form>
    </div>
    <?php
}

//register and define the settings
add_action('admin_init', 'pb_salesmanago_admin_init');
function pb_salesmanago_admin_init()
{
    register_setting(
        'pb_salesmanago_options',
        'pb_salesmanago_options'
    //'pb_salesmanago_validate_option'
    );

    add_settings_section(
        'pb_salesmanago_main',
        __('Settings', 'wco-salesmanago'),
        'pb_salesmanago_section_text',
        'salesmanago'
    );

    add_settings_field(
        'pb_salesmanago_client_id',
        __('Client ID', 'wco-salesmanago'),
        'pb_salesmanago_setting_client_id_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_api_secret',
        __('API Secret', 'wco-salesmanago'),
        'pb_salesmanago_setting_api_secret_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_contact_owner_email',
        __('Contact owner email', 'wco-salesmanago'),
        'pb_salesmanago_setting_email_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_end_point',
        __('Endpoint', 'wco-salesmanago'),
        'pb_salesmanago_setting_end_point_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_tags_register',
        __('Tags after registration', 'wco-salesmanago'),
        'pb_salesmanago_setting_tags_register_input',
        'salesmanago',
        'pb_salesmanago_main'
    );

    add_settings_field(
        'pb_salesmanago_tags_login',
        __('Tags after login', 'wco-salesmanago'),
        'pb_salesmanago_setting_tags_login_input',
        'salesmanago',
        'pb_salesmanago_main'
    );
}

//generate section header
function pb_salesmanago_section_text()
{
    echo '<p>' . __('All necessary information are available at client account SALESmanago in Settings => Integration', 'wco-salesmanago') . '</p>';
}

//display and fill out the contact form
function pb_salesmanago_setting_client_id_input()
{
//download from the database values
    $options = get_option('pb_salesmanago_options');
    $sm_client_id = $options['pb_salesmanago_client_id'];
    //displays a form fields
    echo "<input id='pb_salesmanago_client_id' name='pb_salesmanago_options[pb_salesmanago_client_id]' "
        . "type='text' value='{$sm_client_id}' size=40 />";
}

function pb_salesmanago_setting_api_secret_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_api_secret = $options['pb_salesmanago_api_secret'];
    echo "<input id='pb_salesmanago_api_secret' name='pb_salesmanago_options[pb_salesmanago_api_secret]' "
        . "type='text' value='{$sm_api_secret}' size=40 />";
}

function pb_salesmanago_setting_email_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_email = $options['pb_salesmanago_contact_owner_email'];
    echo "<input id='pb_salesmanago_contact_owner_email' name='pb_salesmanago_options[pb_salesmanago_contact_owner_email]' "
        . "type='text' value='{$sm_email}' size=40 />";
}

function pb_salesmanago_setting_end_point_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_end_point = $options['pb_salesmanago_end_point'];
    echo "<input id='pb_salesmanago_end_point' name='pb_salesmanago_options[pb_salesmanago_end_point]' "
        . "type='text' value='{$sm_end_point}' size=40 />";
}

function pb_salesmanago_setting_tags_register_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_tags = $options['pb_salesmanago_tags_register'];
    echo "<input id='pb_salesmanago_tags_register' name='pb_salesmanago_options[pb_salesmanago_tags_register]' "
        . "type='text' value='{$sm_tags}' size=40 /><br>"
        . "<p>" . __('Tags should be separated by comma without space ex: woocommerce_register,register', 'wco-salesmanago') . "</p>";
}

function pb_salesmanago_setting_tags_login_input()
{
    $options = get_option('pb_salesmanago_options');
    $sm_tags = $options['pb_salesmanago_tags_login'];
    echo "<input id='pb_salesmanago_tags_login' name='pb_salesmanago_options[pb_salesmanago_tags_login]' "
        . "type='text' value='{$sm_tags}' size=40 /><br>"
        . "<p>" . __('Tags should be separated by comma without space ex: woocommerce_login,login', 'wco-salesmanago') . "</p>";
}

function pb_salesmanago_do_post_request($url, $data)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
        array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        )
    );

    return curl_exec($ch);
}

// Create SALESmanago user on Register and Purchase with Register

function pb_salesmanago_create_user($user_id)
{

    $pb_salesmanago_current_user = get_userdata($user_id);
    $pb_salesmanago_user_email = $pb_salesmanago_current_user->user_email;

    $pb_salesmanago_user_id = $pb_salesmanago_current_user->ID;

    $single = true;
    $first_name = 'billing_first_name';
    $last_name = 'billing_last_name';
    $billing_city = 'billing_x_miasto_34046';
    $billing_company = 'billing_company';
    $billing_phone = 'billing_phone';
    $billing_postcode = 'billing_x_kod_pocztowy_96607';
    $billing_street = 'billing_x_adres_77034';
    $billing_street_number = 'billing_address_2';
    $billing_country = 'billing_country';
    $billing_nip = 'billing_nip_85703';
    $billing_if_company = 'billing_zakup_na__7120';
    $billing_newsletter = 'billing_chce_zapisac_sie_na__49144';


    $pb_salesmanago_first_name = get_user_meta($pb_salesmanago_user_id, $first_name, $single);
    $pb_salesmanago_last_name = get_user_meta($pb_salesmanago_user_id, $last_name, $single);
    $pb_salesmanago_name = $pb_salesmanago_first_name . " " . $pb_salesmanago_last_name;
    $pb_salesmanago_city = get_user_meta($pb_salesmanago_user_id, $billing_city, $single);
    $pb_salesmanago_company = get_user_meta($pb_salesmanago_user_id, $billing_company, $single);
    $pb_salesmanago_phone = get_user_meta($pb_salesmanago_user_id, $billing_phone, $single);
    $pb_salesmanago_postcode = get_user_meta($pb_salesmanago_user_id, $billing_postcode, $single);
    $pb_salesmanago_street_first = get_user_meta($pb_salesmanago_user_id, $billing_street, $single);
    $pb_salesmanago_street_second = get_user_meta($pb_salesmanago_user_id, $billing_street_number, $single);
    $pb_salesmanago_street = $pb_salesmanago_street_first . " " . $pb_salesmanago_street_second;
    $pb_salesmanago_country = get_user_meta($pb_salesmanago_user_id, $billing_country, $single);
    $pb_salesmanago_nip = get_user_meta($pb_salesmanago_user_id, $billing_nip, $single);
    $pb_salesmanago_if_company = get_user_meta($pb_salesmanago_user_id, $billing_if_company, $single);
    $pb_salesmanago_newsletter = get_user_meta($pb_salesmanago_user_id, $billing_newsletter, $single);


    $options = get_option('pb_salesmanago_options');
    $sm_client_id = $options['pb_salesmanago_client_id'];
    $sm_api_secret = $options['pb_salesmanago_api_secret'];
    $sm_end_point = $options['pb_salesmanago_end_point'];
    $sm_contact_owner_email = $options['pb_salesmanago_contact_owner_email'];
    $sm_tags = $options['pb_salesmanago_tags_register'];

    $clientId = $sm_client_id;
    $apiKey = 'mfkaqp4fb38j5hb8g9k4k';
    $apiSecret = $sm_api_secret;
    $owner = $sm_contact_owner_email;
    $endpoint = $sm_end_point;

    $smOptIn = $pb_salesmanago_newsletter ? true : false;
    $smOptOut = $pb_salesmanago_newsletter ? false : true;

    $data = array(
        'clientId' => $clientId,
        'apiKey' => $apiKey,
        'requestTime' => time(),
        'sha' => sha1($apiKey . $clientId . $apiSecret),
        'contact' => array(
            'email' => $pb_salesmanago_user_email,
            'company' => $pb_salesmanago_company,
            'name' => $pb_salesmanago_name,
            'phone' => $pb_salesmanago_phone,
            'address' => array(
                'streetAddress' => $pb_salesmanago_street,
                'zipCode' => $pb_salesmanago_postcode,
                'city' => $pb_salesmanago_city,
                'country' => $pb_salesmanago_country
            )

        ),
        'owner' => $owner,
        'forceOptIn' => $smOptIn,
        'forceOptOut' => $smOptOut,
        'tags' => explode(',', $sm_tags),
        'properties' => array(
            'nip' => $pb_salesmanago_nip,
            'firma' => $pb_salesmanago_if_company
        )
    );

    $json = json_encode($data);

    if ($result = pb_salesmanago_do_post_request('http://' . $endpoint . '/api/contact/upsert', $json)) {
        $r = json_decode($result);

        $contactId = $r->{'contactId'};

        setcookie('smclient', $contactId, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);

        return $r;
    }

}

//
add_action('woocommerce_checkout_update_user_meta', 'pb_salesmanago_create_user');
add_action('woocommerce_customer_save_address', 'pb_salesmanago_create_user');
add_action('user_register', 'pb_salesmanago_create_user');

//Update SALESmanago user data on Login

function pb_salesmanago_login_user($user_login)
{

    $pb_salesmanago_user_login = get_user_by('login', $user_login);
    $pb_salesmanago_user_id = $pb_salesmanago_user_login->ID;
    $pb_salesmanago_user_email = $pb_salesmanago_user_login->user_email;

    $single = true;
    $first_name = 'billing_first_name';
    $last_name = 'billing_last_name';
    $billing_city = 'billing_x_miasto_34046';
    $billing_company = 'billing_company';
    $billing_phone = 'billing_phone';
    $billing_postcode = 'billing_x_kod_pocztowy_96607';
    $billing_street = 'billing_x_adres_77034';
    $billing_street_number = 'billing_address_2';
    $billing_country = 'billing_country';
    $billing_nip = 'billing_nip_85703';
    $billing_if_company = 'billing_zakup_na__7120';
    $billing_newsletter = 'billing_chce_zapisac_sie_na__49144';

    $pb_salesmanago_first_name = get_user_meta($pb_salesmanago_user_id, $first_name, $single);
    $pb_salesmanago_last_name = get_user_meta($pb_salesmanago_user_id, $last_name, $single);
    $pb_salesmanago_name = $pb_salesmanago_first_name . " " . $pb_salesmanago_last_name;
    $pb_salesmanago_city = get_user_meta($pb_salesmanago_user_id, $billing_city, $single);
    $pb_salesmanago_company = get_user_meta($pb_salesmanago_user_id, $billing_company, $single);
    $pb_salesmanago_phone = get_user_meta($pb_salesmanago_user_id, $billing_phone, $single);
    $pb_salesmanago_postcode = get_user_meta($pb_salesmanago_user_id, $billing_postcode, $single);
    $pb_salesmanago_street_first = get_user_meta($pb_salesmanago_user_id, $billing_street, $single);
    $pb_salesmanago_street_second = get_user_meta($pb_salesmanago_user_id, $billing_street_number, $single);
    $pb_salesmanago_street = $pb_salesmanago_street_first . " " . $pb_salesmanago_street_second;
    $pb_salesmanago_country = get_user_meta($pb_salesmanago_user_id, $billing_country, $single);
    $pb_salesmanago_nip = get_user_meta($pb_salesmanago_user_id, $billing_nip, $single);
    $pb_salesmanago_if_company = get_user_meta($pb_salesmanago_user_id, $billing_if_company, $single);
    $pb_salesmanago_newsletter = get_user_meta($pb_salesmanago_user_id, $billing_newsletter, $single);

    $options = get_option('pb_salesmanago_options');
    $sm_client_id = $options['pb_salesmanago_client_id'];
    $sm_api_secret = $options['pb_salesmanago_api_secret'];
    $sm_end_point = $options['pb_salesmanago_end_point'];
    $sm_contact_owner_email = $options['pb_salesmanago_contact_owner_email'];
    $sm_tags = $options['pb_salesmanago_tags_login'];

    $clientId = $sm_client_id;
    $apiKey = 'mfkaqp4fb38j5hb8g9k4k';
    $apiSecret = $sm_api_secret;
    $owner = $sm_contact_owner_email;
    $endpoint = $sm_end_point;

    $smOptIn = $pb_salesmanago_newsletter ? true : false;
    $smOptOut = $pb_salesmanago_newsletter ? false : true;

    $data = array(
        'clientId' => $clientId,
        'apiKey' => $apiKey,
        'requestTime' => time(),
        'sha' => sha1($apiKey . $clientId . $apiSecret),
        'contact' => array('email' => $pb_salesmanago_user_email,
            'company' => $pb_salesmanago_company,
            'name' => $pb_salesmanago_name,
            'phone' => $pb_salesmanago_phone,
            'address' => array(
                'streetAddress' => $pb_salesmanago_street,
                'zipCode' => $pb_salesmanago_postcode,
                'city' => $pb_salesmanago_city,
                'country' => $pb_salesmanago_country
            )
        ),
        'owner' => $owner,
        'forceOptIn' => $smOptIn,
        'forceOptOut' => $smOptOut,
        'tags' => explode(',', $sm_tags),
        'properties' => array(
            'nip' => $pb_salesmanago_nip,
            'firma' => $pb_salesmanago_if_company
        )
    );


    $json = json_encode($data);

    if ($result = pb_salesmanago_do_post_request('http://' . $endpoint . '/api/contact/upsert', $json)) {
        $r = json_decode($result);

        $contactId = $r->{'contactId'};

        setcookie('smclient', $contactId, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);
    }
}

add_action('wp_login', 'pb_salesmanago_login_user');

//set client id cookie on login

function pb_salesmanago_cookie_id($user_login)
{

    $pb_salesmanago_user_login = get_user_by('login', $user_login);
    $pb_salesmanago_user_id = $pb_salesmanago_user_login->ID;

    setcookie('pbsmcookieid', $pb_salesmanago_user_id, time() + 3600, '/', COOKIE_DOMAIN, false);

}

add_action('wp_login', 'pb_salesmanago_cookie_id');

//delete client id cookie on logout

function pb_salesmanago_cookie_id_delete()
{

    if (isset ($_COOKIE['pbsmcookieid'])) {

        setcookie('pbsmcookieid', "", time() - 3600, '/', COOKIE_DOMAIN, false);
    }
}

add_action('wp_logout', 'pb_salesmanago_cookie_id_delete');

//Add SALESmanago user external event on Purchase

function pb_salesmanago_purchase($order)
{

    global $woocommerce;

    $pb_salesmanago_purchase_order_id = $order->id;

    $pb_salesmanago_order = new WC_Order($pb_salesmanago_purchase_order_id);

    $pb_items = $pb_salesmanago_order->get_items();

    $currentURL = add_query_arg($woocommerce->query_string, '', home_url($woocommerce->request));
    $pb_items_array = array();
    $pb_items_names_array = array();

    foreach ($pb_items as $item) {

        $pb_items_array[] = $item['product_id'];
        $pb_items_names_array[] = $item['name'];
    }

    $pb_salesmanago_products_ids = implode(",", $pb_items_array);
    $pb_salesmanago_products_names = implode(",", $pb_items_names_array);

    $single = true;

    $purchase_billing_email = '_billing_email';
    $purchase_order_total = '_order_total';
    $purchase_payment_method = '_payment_method_title';

    $pb_salesmanago_purchase_billing_email = get_post_meta($pb_salesmanago_purchase_order_id, $purchase_billing_email, $single);
    $pb_salesmanago_purchase_order_total = get_post_meta($pb_salesmanago_purchase_order_id, $purchase_order_total, $single);
    $pb_salesmanago_purchase_payment_method = get_post_meta($pb_salesmanago_purchase_order_id, $purchase_payment_method, $single);

    $options = get_option('pb_salesmanago_options');
    $sm_client_id = $options['pb_salesmanago_client_id'];
    $sm_api_secret = $options['pb_salesmanago_api_secret'];
    $sm_end_point = $options['pb_salesmanago_end_point'];
    $sm_contact_owner_email = $options['pb_salesmanago_contact_owner_email'];

    $clientId = $sm_client_id;
    $apiKey = 'mfkaqp4fb38j5hb8g9k4k';
    $apiSecret = $sm_api_secret;
    $owner = $sm_contact_owner_email;
    $endpoint = $sm_end_point;

    $dt = new DateTime('NOW');

    $data = array(
        'clientId' => $clientId,
        'apiKey' => $apiKey,
        'requestTime' => time(),
        'sha' => sha1($apiKey . $clientId . $apiSecret),
        'owner' => $owner,
        'contactEvent' => array(
            'date' => $dt->format('c'),
            'description' => $pb_salesmanago_purchase_payment_method,
            'products' => $pb_salesmanago_products_ids,
            'detail1' => $pb_salesmanago_products_names,
            'value' => $pb_salesmanago_purchase_order_total,
            'location' => $currentURL,
            'contactExtEventType' => "PURCHASE",
            'externalId' => $pb_salesmanago_purchase_order_id
        )
    );

    $data['email'] = $pb_salesmanago_purchase_billing_email;
    $json = json_encode($data);
    $result = pb_salesmanago_do_post_request('http://' . $endpoint . '/api/contact/addContactExtEvent', $json);
    $r = json_decode($result);
    setcookie('SMeventID', "", time() - 3600, '/', COOKIE_DOMAIN, false);
}

add_action('woocommerce_order_details_after_order_table', 'pb_salesmanago_purchase', 1);

//Create SALESmanago user on Purchase without register

function pb_salesmanago_no_account_purchase_create_user($order)
{

    if (!is_user_logged_in()) {

        global $woocommerce;

        $pbSm_no_account_purchase_order_id = $order->id;


        setcookie('pbsmorderid', $pbSm_no_account_purchase_order_id, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);

        $single = true;

        $purchase_no_account_email = '_billing_email';
        $purchase_no_account_company = '_billing_company';
        $purchase_no_account_first_name = '_billing_first_name';
        $purchase_no_account_last_name = '_billing_last_name';
        $purchase_no_account_phone = '_billing_phone';
        $purchase_no_account_address_1 = '_billing_x_adres_77034';
        $purchase_no_account_address_2 = '_billing_address_2';
        $purchase_no_account_postcode = '_billing_x_kod_pocztowy_96607';
        $purchase_no_account_city = '_billing_x_miasto_34046';
        $purchase_no_account_country = '_billing_country';
        $purchase_no_account_nip = '_billing_nip_85703';
        $purchase_no_account_if_company = '_billing_zakup_na__7120';
        $purchase_no_account_newsletter = 'billing_chce_zapisac_sie_na__49144';

        $pbSM_purchase_no_account_email = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_email, $single);
        $pbSM_purchase_no_account_company = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_company, $single);
        $pbSM_purchase_no_account_first_name = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_first_name, $single);
        $pbSM_purchase_no_account_last_name = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_last_name, $single);
        $pbSM_purchase_no_account_name = $pbSM_purchase_no_account_first_name . " " . $pbSM_purchase_no_account_last_name;
        $pbSM_purchase_no_account_phone = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_phone, $single);
        $pbSM_purchase_no_account_address_1 = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_address_1, $single);
        $pbSM_purchase_no_account_address_2 = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_address_2, $single);
        $pbSM_purchase_no_account_address = $pbSM_purchase_no_account_address_1 . " " . $pbSM_purchase_no_account_address_2;
        $pbSM_purchase_no_account_postcode = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_postcode, $single);
        $pbSM_purchase_no_account_city = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_city, $single);
        $pbSM_purchase_no_account_country = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_country, $single);
        $pbSM_purchase_no_account_nip = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_nip, $single);
        $pbSM_purchase_no_account_if_company = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_if_company, $single);
        $pbSM_purchse_no_account_newsletter = get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_newsletter, $single);

        $options = get_option('pb_salesmanago_options');
        $sm_client_id = $options['pb_salesmanago_client_id'];
        $sm_api_secret = $options['pb_salesmanago_api_secret'];
        $sm_end_point = $options['pb_salesmanago_end_point'];
        $sm_tags = $options['pb_salesmanago_tags_register'];
        $sm_contact_owner_email = $options['pb_salesmanago_contact_owner_email'];

        $clientId = $sm_client_id;
        $apiKey = 'mfkaqp4fb38j5hb8g9k4k';
        $apiSecret = $sm_api_secret;
        $owner = $sm_contact_owner_email;
        $endpoint = $sm_end_point;

        $smOptIn = $pbSM_purchse_no_account_newsletter ? true : false;
        $smOptOut = $pbSM_purchse_no_account_newsletter ? false : true;

        $data = array(
            'clientId' => $clientId,
            'apiKey' => $apiKey,
            'requestTime' => time(),
            'sha' => sha1($apiKey . $clientId . $apiSecret),
            'contact' => array(
                'email' => $pbSM_purchase_no_account_email,
                'company' => $pbSM_purchase_no_account_company,
                'name' => $pbSM_purchase_no_account_name,
                'phone' => $pbSM_purchase_no_account_phone,
                'address' => array(
                    'streetAddress' => $pbSM_purchase_no_account_address,
                    'zipCode' => $pbSM_purchase_no_account_postcode,
                    'city' => $pbSM_purchase_no_account_city,
                    'country' => $pbSM_purchase_no_account_country
                )
            ),
            'owner' => $owner,
            'forceOptIn' => $smOptIn,
            'forceOptOut' => $smOptOut,
            'tags' => explode(',', $sm_tags),
            'async' => false,
            'properties' => array(
                'nip' => $pbSM_purchase_no_account_nip,
                'firma' => $pbSM_purchase_no_account_if_company
            )
        );

        $json = json_encode($data);

        if ($result = pb_salesmanago_do_post_request('http://' . $endpoint . '/api/contact/upsert', $json)) {
            $r = json_decode($result);
            $contactId = $r->{'contactId'};
            setcookie('smclient', $contactId, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);
        }
    }
}

add_action('woocommerce_order_details_after_order_table', 'pb_salesmanago_no_account_purchase_create_user', 0);

//Add SALESmanago user external event on Cart

function pb_salesmanago_add_to_cart()
{

    global $woocommerce;

    $pb_cookie_id = get_current_user_id();
    $pb_cart = $woocommerce->cart->get_cart();

    $currentURL = add_query_arg($woocommerce->query_string, '', home_url($woocommerce->request));
    $productsArray = array();
    $productsNameArray = array();
    $cart_total_price = 0;

    foreach ($pb_cart as $product) {
        $productsArray[] = $product['product_id'];
        $productsNameArray[] = $product['data']->name;
        $cart_total_price = $cart_total_price + ($product['quantity'] * $product['data']->price);
    }
    $productsIds = implode(",", $productsArray);
    $productsNames = implode(",", $productsNameArray);
    $pb_salesmanago_user_id = get_user_by('id', $pb_cookie_id);

    $options = get_option('pb_salesmanago_options');
    $sm_client_id = $options['pb_salesmanago_client_id'];
    $sm_api_secret = $options['pb_salesmanago_api_secret'];
    $sm_end_point = $options['pb_salesmanago_end_point'];
    $sm_contact_owner_email = $options['pb_salesmanago_contact_owner_email'];

    $clientId = $sm_client_id;
    $apiKey = 'mfkaqp4fb38j5hb8g9k4k';
    $apiSecret = $sm_api_secret;
    $owner = $sm_contact_owner_email;
    $endpoint = $sm_end_point;

    $dt = new DateTime('NOW');

    $data = array(
        'clientId' => $clientId,
        'apiKey' => $apiKey,
        'requestTime' => time(),
        'sha' => sha1($apiKey . $clientId . $apiSecret),
        'owner' => $owner,
        'contactEvent' => array(
            'date' => $dt->format('c'),
            'description' => 'Cart',
            'products' => $productsIds,
            'value' => $cart_total_price,
            'detail1' => $productsNames,
            'location' => $currentURL,
            'contactExtEventType' => "CART"
        )
    );

    if (isset($_COOKIE['SMeventID']) && !empty($_COOKIE['SMeventID'])) {
        $data['contactEvent']['eventId'] = $_COOKIE['SMeventID'];
        $json = json_encode($data);
        pb_salesmanago_do_post_request('http://' . $endpoint . '/api/contact/updateContactExtEvent', $json);
    } else {
        if (is_user_logged_in()) {
            $data['email'] = $pb_salesmanago_user_id->user_email;
        } elseif (isset($_COOKIE['smclient']) && !empty($_COOKIE['smclient'])) {
            $data['contactId'] = $_COOKIE['smclient'];
        }

        if (!empty($data['email']) || !empty($data['contactId'])) {
            $json = json_encode($data);

            if ($result = pb_salesmanago_do_post_request('http://' . $endpoint . '/api/contact/addContactExtEvent', $json)) {
                $r = json_decode($result);
                $eventId = $r->{'eventId'};
                setcookie('SMeventID', $eventId, time() + 3650 * 86400, '/', COOKIE_DOMAIN, false);
            }
        }
    }
}

add_action('woocommerce_add_to_cart', 'pb_salesmanago_add_to_cart');
add_action('woocommerce_update_cart_action_cart_updated', "pb_salesmanago_add_to_cart");

function post_user_sm_without_create_checkbox($order)
{
    $user_id = get_current_user_id();

    if ($user_id == 0 || $user_id == null) {
        pb_salesmanago_no_account_purchase_create_user($order);
    }
}

add_action('woocommerce_order_details_after_order_table', 'post_user_sm_without_create_checkbox');