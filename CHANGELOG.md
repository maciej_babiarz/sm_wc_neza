WooCommerce integration plugin v 1.0.1
- Purchase event fix
- woocommerce_order_details_after_customer_details hook depreciated

##

WooCommerce integration plugin v 1.0.0
- Initial release in Wordpress Plugins Shop

##

WooCommerce integration plugin v 2.2.4
Fix:
- Dynamic endpoint adding,

Changes:
- New monitoring code,

##

WooCommerce integration plugin v 2.2.3
Fix:
- Adding new items to the same cart updates event (SMeventId cookie),
- Purchase without checked "create_account" checkbox adds event in sm, 

##
WooCommerce integration plugin v 2.2.2
Fix:
- Creating SM contact when "create_account" checkbox is uncheck in wordpress

##
WooCommerce integration plugin v 1.0.0
Added:
- CART is registered for guests (with smclient cookie)
