(function(w, r, a, sm, s ) {
    w['SalesmanagoObject'] = r;
    w[r] = w[r] || function () {( w[r].q = w[r].q || [] ).push(arguments)};
    sm = document.createElement('script'); sm.type = 'text/javascript'; sm.async = true; sm.src = a;
    s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(sm, s);
})(window, 'sm', ('https:' == document.location.protocol ? 'https://' : 'http://') + _smEndpoint +'/static/sm.js');